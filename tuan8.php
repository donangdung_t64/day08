<?php
    $_COOKIE
?>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>
        <style>
            * {
                box-sizing: border-box;
            }

            body {
                background-color: whitesmoke;
                width: 1170px;
            }

            header {
                max-width: 700px;
                position: relative;
                background-color: white;
            }
            
            button {
                height: 30px;
                width: 100px;
                background-color: #aad4ff;
            }

            .facul {
                display: flex;
            }
            
            .facul select#slfacul {
                width: 200px;
                color: rgb(32,32,32);
                height: 30px;
                margin-left: 50px;
            }
            .tukhoa {
                margin-top: 20px;
            }
            input {
                height: 30px;
                width: 200px;
                margin-left: 50px;
            }
            .search{
                margin-left: 200px;
                margin-top: 20px;
            }
            h3 {
                display: flex;
            }
            .add {
                margin-left: 380px;
            }
            .bodyer {
                max-width: 1000px;
            }
            .bodyer .add {
                align-items: flex-end;
            }
            ul#tenmuc {
                list-style: none;
                display: flex;
            }

            ul#tenmuc li {
                margin-right: 80px;
                align-items: center;
            }
            ul#tenmuc li:last-child {
                margin-left: 110px;
            }
            .No {
                display: flex;
            }
            ul.NO {
                list-style: none;
                display: block;  
            }
            ul.NO li{
                margin-right: 20px;
                margin-top: 10px;
            }
            ul.NO li:first-child {
                margin-top: 5px;
            }
            ul.NO li.action {
                display: flex;
            }
            ul.NO li:last-child {
                margin-right: 0px;
            }
            ul.bt {
                list-style: none;
            }
        </style>
        <header>
            <div class="facul">
                <div class="faculty">
                    <button>Khoa</button>
                </div>
                <form action="#">            
                    <select name="slfaculty" id="slfacul">
                        <option></option>
                        <option>Khoa học máy tính</option>
                        <option>Khoa học vật liệu</option>
                    </select>
                    <span class="tamgiac">
                        <i class="fa-sharp fa-solid fa-caret-down"></i>
                    </span>
                </form>
            </div>
            <div class="tukhoa">
                <button>Tu khoa</button>
                <input type="text">
            </div>
            <div class="search">
                <button>Tim kiem</button>
            </div>
        </header>
        <h3>So sinh vien tim thay: XX
            <div class="add">
                <button>Them</button>
            </div>
        </h3>
        <div class="bodyer">
            <div class="tieude">
                <ul id="tenmuc">
                    <li>No</li>
                    <li>Ten sinh vien</li>
                    <li>Khoa</li>
                    <li>Action</li>
                </ul>
            </div>
            <div class="No">
                <ul class="NO">
                    <li>1</li>
                    <li>2</li>
                    <li>3</li>
                    <li>4</li>
                </ul>
                <ul class="NO">
                    <li>Nguyen Van A</li>
                    <li>Tran Thi B</li>
                    <li>Nguyen Hoang C</li>
                    <li>Dinh Quang D</li>
                </ul>
                <ul class="NO">
                    <li>Khoa hoc may tinh</li>
                    <li>Khoa hoc vat lieu</li>
                    <li>Khoa hoc may tinh</li>
                    <li>Khoa hoc vat lieu</li>
                </ul>
                <ul class="bt">
                    <li class="action">
                        <button>Xoa</button>
                        <button>Sua</button>
                    </li>
                    <li class="action">
                        <button>Xoa</button>
                        <button>Sua</button>
                    </li>
                    <li class="action">
                        <button>Xoa</button>
                        <button>Sua</button>
                    </li>
                    <li class="action">
                        <button>Xoa</button>
                        <button>Sua</button>
                    </li>
                </ul>
            </div>
        </div>
    </body>
    <script src=""></script>
</html>